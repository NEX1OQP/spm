import { Injectable } from "@angular/core";
import { AppConfiguration } from "./app-configuration";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class JsonAppConfigService extends AppConfiguration {
  constructor(private httClient: HttpClient) {
    super();
  }

  load() {
    return this.httClient
      .get<AppConfiguration>("./app.config.json")
      .toPromise()
      .then((data) => {
        this.apiBaseUrl = data?.apiBaseUrl;
        this.title = data?.title;
        this.copyRight = data?.copyRight;
        this.version = data?.version;
        this.secondsToStartServer = data?.secondsToStartServer;
        this.secondsToStopServer = data?.secondsToStopServer;
        this.refreshRate = data?.refreshRate;
        this.openWindowTo=data?.openWindowTo;
        this.apiAuthenticate = data?.apiAuthenticate;
        this.apiServerList = data?.apiServerList;
        this.apiStartServe = data?.apiStartServe;
        this.apiStopServe = data?.apiStopServe;
      })
      .catch(() => {
        console.error("Could not load configuration file");
      });
  }
}
