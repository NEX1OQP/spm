export abstract class AppConfiguration {
  title?: string;
  apiBaseUrl?: string;
  copyRight?: string;
  version?: string;
  secondsToStartServer?:number;
  secondsToStopServer?:number;
  refreshRate?:number;
  openWindowTo?:string;
  apiAuthenticate?: string;
  apiServerList?: string;
  apiStartServe?:string;
  apiStopServe?:string;
}
