export enum AppRoute {
  Login = '',        // Default Route
  HomePage = 'home', // Server List Screen
  Default = '**',    // Default Screen if route not found
}

export enum ProcessToRun {
  StartServer = 0,       
  StopServer = 1
}
