import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { Subscription } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AppConfiguration } from "@app/config/app-configuration";
import { Server } from "@app/models/server.model";
import { ServerService } from "@app/services/serverService.service";
import { User } from "@app/models/user.model";
import { ProcessToRun } from "@app/enums/app-enums";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit, OnDestroy {
  serverOb$!: Subscription;
  serverList: Server[] = [];
  user: User;

  @ViewChild("verificationDialog") verificationDlgRef: ElementRef;
  @ViewChild("messageDialog") messageDlgRef: ElementRef;

  totalSeconds: number = this.appConfiguration.secondsToStartServer;
  refreshRateSeconds: number = this.appConfiguration.refreshRate;
  openWindowTo: string = this.appConfiguration.openWindowTo;

  intervalSeconds: number = 1;
  currectSeconds: number = 0;
  currentProcessId: number = -1;
  processStarted: boolean = false;
  redirectUrl: string;
  dialogMsg: string;
  progressBarCaption: string;
  refreshIntervalId = null;
  statusIntervalId = null;

  constructor(
    private serverService: ServerService,
    private appConfiguration: AppConfiguration,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    document.body.style.cursor = "wait";

    this.user = JSON.parse(sessionStorage.getItem("spm-auth"));
    this.loadServerData();
    this.setupRefreshServerList();
  }

  setupRefreshServerList() {
    this.refreshIntervalId = setInterval(() => {
      this.loadServerData();
    }, this.refreshRateSeconds * 1000);
  }

  public processToRunEnums() {
    return ProcessToRun;
  }

  loadServerData() {
    document.body.style.cursor = "wait";

    this.serverOb$ = this.serverService.getServers().subscribe({
      next: (data) => {
        if (data.statusCode == 200 && data !== undefined && data !== null) {
          this.serverList = data.body.sort((a, b) =>
            a.status.toLocaleLowerCase() < b.status.toLocaleLowerCase() ? -1 : 1
          );
        }
      },
      error: (error: HttpErrorResponse) => {
        if (error.status == 404) {
        }
      },
      complete: () => {
        document.body.style.cursor = "default";
      },
    });
  }

  startProcess(server) {
    document.body.style.cursor = "wait";
    this.progressBarCaption = "Starting Server";

    this.processStarted = true;
    this.currectSeconds = 0;

    this.startServerInstance(server);
  }

  redirectToServerInstance(server) {
    server.showProgressBar = false;

    this.showStartButton(server);

    window.open(this.redirectUrl.toString(), this.openWindowTo);
  }

  showStartButton(server) {
    return server.status == "Available" && server.hideButton != true;
  }

  showStopServerButton(server) {
    return server.status == "In Use";
  }
  stopProcess(server) {
    document.body.style.cursor = "wait";

    server.status = "Not Available";

    this.processStarted = true;
    this.currectSeconds = 0;
    this.progressBarCaption = "Stopping Server";

    this.stopServerInstance(server);
  }

  stopServerInstance(server) {
    //This is for testing
    //this.setupProgressbar(server);

    this.serverOb$ = this.serverService
      .stopStartServer(this.user.email.trim(), server.instanceId.trim())
      .subscribe({
        next: (data) => {
          if (data.statusCode == 200) {
            document.body.style.cursor = "default";

            this.setupProgressbar(server);
          }
        },
        error: (error: HttpErrorResponse) => {
          this.processStarted = false;
          document.body.style.cursor = "default";
          alert(error.message);
        },
        complete: () => {},
      });
  }

  startServerInstance(server) {
    // This is for testing
    // this.setupProgressbar(server);
    // server.status = "In Use";
    // server.allocate = this.user.email.trim().toLocaleLowerCase();

    this.serverOb$ = this.serverService
      .startServer(this.user.email.trim(), server.instanceId.trim())
      .subscribe({
        next: (data) => {
          if (data.statusCode == 200) {
            document.body.style.cursor = "default";

            this.redirectUrl = "http://" + data.body;
            server.status = "In Use";
            server.allocate = this.user.email.toLocaleLowerCase();

            this.setupProgressbar(server);
          }
        },
        error: (error: HttpErrorResponse) => {
          this.processStarted = false;
          document.body.style.cursor = "default";
          alert(error.message);
        },
        complete: () => {},
      });
  }

  setupProgressbar(server) {
    server.widthStyle = "0%";
    server.progressPercent = 0;
    server.showProgressBar = true;

    if (this.currentProcessId == ProcessToRun.StartServer) {
      this.totalSeconds = this.appConfiguration.secondsToStartServer;
    } else if (this.currentProcessId == ProcessToRun.StopServer) {
      this.totalSeconds = this.appConfiguration.secondsToStopServer;
    }

    // Start Interval
    this.statusIntervalId = setInterval(() => {
      this.currectSeconds += 1;
      server.progressPercent = Math.round(
        (this.currectSeconds / this.totalSeconds) * 100
      );
      server.widthStyle = server.progressPercent + "%";
    }, this.intervalSeconds * 1000);

    // Clean Up Interval
    setTimeout(() => {
      clearInterval(this.statusIntervalId);

      document.body.style.cursor = "default";
      this.processStarted = false;

      if (this.currentProcessId == ProcessToRun.StartServer) {
        this.redirectToServerInstance(server);
      } else if (this.currentProcessId == ProcessToRun.StopServer) {
        this.loadServerData();
      }
    }, this.totalSeconds * 1000);
  }

  cleanup() {
    if (this.serverOb$ != null && this.serverOb$ != undefined) {
      this.serverOb$.unsubscribe();
    }

    if (this.refreshIntervalId != null && this.refreshIntervalId != undefined) {
      clearInterval(this.refreshIntervalId);
    }

    if (this.statusIntervalId != null && this.statusIntervalId != undefined) {
      clearInterval(this.statusIntervalId);
    }
  }

  ngOnDestroy(): void {
    this.cleanup();
  }

  confirmToStartServer(server) {
    this.dialogMsg = "Are you sure you wish to START server?";

    this.modalService.open(this.verificationDlgRef).result.then((result) => {
      if (result == "OK clicked") {
        this.currentProcessId = ProcessToRun.StartServer;
        this.startProcess(server);
      }
    });
  }

  confirmToStopServer(server) {
    let currentUserEmail = this.user.email.trim().toLocaleLowerCase();
    let allocatedUser = server.allocate.trim().toLocaleLowerCase();

    // Only the allocated user can stop their process.  They cannot not stop another users process
    if (allocatedUser != currentUserEmail) {
      this.dialogMsg = "Only the 'Allocated To' user can stop this server.";

      this.modalService.open(this.messageDlgRef).result.then((result) => {});
    } else {
      this.dialogMsg = "Are you sure you wish to STOP server?";

      this.modalService.open(this.verificationDlgRef).result.then((result) => {
        if (result == "OK clicked") {
          this.currentProcessId = ProcessToRun.StopServer;
          this.stopProcess(server);
        }
      });
    }
  }
}
