import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { AppConfiguration } from "../config/app-configuration";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-type": "application/json",
  }),
};

@Injectable({
  providedIn: "root",
})
export class ServerService {
  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfiguration
  ) {}

  getServers(): Observable<any> {
    let url = this.appConfiguration.apiServerList;

    return this.http.get<any>(url);
  }

  startServer(email: string, instanceId: string): Observable<any> {
    let url = this.appConfiguration.apiStartServe;

    url = url.replace("@email", email).replace("@instanceId", instanceId);

    return this.http.get<any>(url);
  }

  stopStartServer(email: string, instanceId: string): Observable<any> {
    let url = this.appConfiguration.apiStopServe;

    url = url.replace("@email", email).replace("@instanceId", instanceId);

    return this.http.get<any>(url);
  }
}
